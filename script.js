function kvadranteRivnyanna(a, b, c){
    function result(a1, b1, d1){
        return ((-b1 + d1) / (2 * a1))
    }
    let D = ((b * b) - (4 * a * c));
    console.log(D)
    if (D < 0)
        return 'рівняння не має розвязків';
    if (D === 0)
        return ([result(a, b, Math.sqrt(D))]);
    return ([result(a, b, Math.sqrt(D)), result(a, b, -Math.sqrt(D))]);
}

console.log(kvadranteRivnyanna(2, 4,2))